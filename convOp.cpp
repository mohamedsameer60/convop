#include <iostream>

using namespace std;

void conv(int input[][3][3],int filter[][3][2][2],int stride_height,int stride_width)
{
    int i,j,k,l,x=0,y=0;
    int output[3][2][2];
    int sum=0;
    
    for(i=0;i<2;i=i+stride_height)
    {
        for(j=0;j<2;j=j+stride_width)
        {
            sum = 0;
            for(k=0;k<2;k++)
            {
                for(l=0;l<2;l++)
                {
                    sum = sum + (input[i][j][k] * filter[0][j][k][l]); 
                }
            }
            output[j][k][l] = sum;
        }
    }
    
    printf("The ouput matrix:\n");
    
    for(i=0;i<2;i++)
    {
        for(j=0;j<2;j++)
        {
            for(k=0;k<2;k++)
            {
                printf("%d ",output[i][j][k]);
            }
        }
        printf("\n");
    }
}

int main()
{
    
    int input[3][3][3] = {
        
                          { 
                           {1,2,3},
                           {4,5,6},
                           {7,8,9}
                         },
                         
                         { 
                           {10,11,12},
                           {13,14,15},
                           {16,17,18}
                         },
                         
                         { 
                           {5,7,2},
                           {3,5,8},
                           {9,6,1}
                         },
    
     };
    
    int stride_height = 1;
    int stride_width = 1;
    
    int filter[1][3][2][2] = {
                              {
                               {
                                {1,0},
                                {0,1}
                               },
                               
                               {
                                {1,0},
                                {0,1}
                               },
                               
                               {
                                {1,0},
                                {0,1}
                               },
                             }
        
    };
                              
    
    conv(input,filter,stride_height,stride_width);
    return 0;
}
